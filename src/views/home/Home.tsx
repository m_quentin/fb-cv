import React from 'react';
import { Link } from 'react-router-dom';
import './Home.scss';

const Home = () => {
    return (
        <div className="container">
            <div className="home-banner"></div>
        </div>
    );
};

export default Home;
