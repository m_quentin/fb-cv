import React, { useState } from 'react';
import { NavLink, useNavigate } from 'react-router-dom';
import './Navbar.scss';
import logo from '../../assets/fb-logo/48.svg';
import back from '../../assets/arrows/left.png';
import home from '../../assets/menu/home.svg';
import book from '../../assets/menu/book.png';
import id from '../../assets/menu/identity.jpg';
import enterprise from '../../assets/menu/enterprise.png';
import portfolio from '../../assets/menu/portfolio.svg';
import arrow_right from '../../assets/arrows/right.png';

const Navbar = () => {
    const searchItems = [
        { name: 'Home', route: '/' },
        { name: 'Accueil', route: '/' },
        { name: 'Experiences', route: '/experiences' },
        { name: 'Education', route: '/education' },
        { name: 'Formation', route: '/education' },
        { name: 'Ecole', route: '/education' },
        { name: 'Portfolio', route: '/portfolio' },
        { name: 'Hobbies', route: '/account' },
        { name: 'Language', route: '/account' },
        { name: 'Langues', route: '/account' },
        { name: 'Skills', route: '/account' },
        { name: 'Compétences', route: '/account' },
    ];
    const [focused, setFocused] = useState(false);
    const [displayedItems, setdisplayedItems] = useState(searchItems);
    const navigate = useNavigate();

    return (
        <div className="wrapper">
            <div className="logo">
                {focused ? (
                    <>
                        <div className="autocomplete-items">
                            <div></div>
                            {displayedItems.map((item) => (
                                <div
                                    onClick={() => {
                                        navigate(item.route);
                                        setFocused(false);
                                    }}
                                >
                                    <span>{item.name}</span>
                                    <img src={arrow_right} alt="go" />
                                </div>
                            ))}
                        </div>
                        <span className="fb-arrow">
                            <img src={back} alt="Back" onClick={() => setFocused(false)} />
                        </span>
                        <form autoComplete="off">
                            <div className="autocomplete">
                                <input
                                    className="search-input"
                                    type="text"
                                    name="search"
                                    placeholder="Rechercher sur Facebook"
                                    autoFocus
                                    onBlur={() => setFocused(false)}
                                    onChange={(e) =>
                                        setdisplayedItems(
                                            searchItems.filter((item) =>
                                                item.name
                                                    .toLocaleLowerCase()
                                                    .includes(e.target.value.toLocaleLowerCase()),
                                            ),
                                        )
                                    }
                                />
                            </div>
                        </form>
                    </>
                ) : (
                    <>
                        <NavLink to={'/'}>
                            <img src={logo} alt="Facebook Logo" className="fb-logo" />
                        </NavLink>
                        <div style={{ width: '300px' }}>
                            <label className="search-label">
                                <input
                                    type="text"
                                    placeholder="Rechercher sur Facebook"
                                    onFocus={() => setFocused(true)}
                                />
                            </label>
                        </div>
                    </>
                )}
            </div>
            <div className="main-nav">
                <ul className="menu">
                    <NavLink to={'/'}>
                        <li>
                            <img src={home} alt="Home" className="menu-logo" />
                        </li>
                    </NavLink>
                    <NavLink to={'/education'}>
                        <li>
                            <img src={book} alt="Education" className="menu-logo" />
                        </li>
                    </NavLink>
                    <NavLink to={'/experiences'}>
                        <li>
                            <img src={enterprise} alt="Experiences" className="menu-logo" />
                        </li>
                    </NavLink>
                    <NavLink to={'/portfolio'}>
                        <li>
                            <img src={portfolio} alt="Portfolio" className="menu-logo" />
                        </li>
                    </NavLink>
                </ul>
            </div>
            <div className="account">
                <NavLink to={'/account'}>
                    <img src={id} alt="ID" className="menu-logo" />
                </NavLink>
            </div>
        </div>
    );
};

export default Navbar;
