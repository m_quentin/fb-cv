import React from 'react';
import { BrowserRouter as Router, Navigate, Route, Routes } from 'react-router-dom';
import './App.scss';
import Navbar from './components/navbar/Navbar';
import Account from './views/account/Account';
import Experiences from './views/experiences/Experiences';
import Education from './views/education/Education';
import Home from './views/home/Home';
import Portfolio from './views/portfolio/Portfolio';

const App = () => {
    return (
        <Router>
            <Navbar />
            <Routes>
                <Route path="/" element={<Home />} />
                <Route path="/education" element={<Education />} />
                <Route path="/experiences" element={<Experiences />} />
                <Route path="/portfolio" element={<Portfolio />} />
                <Route path="/account" element={<Account />} />
                <Route path="*" element={<Navigate to="/" />} />
            </Routes>
        </Router>
    );
};

export default App;
